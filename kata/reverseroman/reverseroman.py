"""Reverse Roman modul"""

from enum import Enum
import re

class ROMAN_NUM(Enum):
    I = 1
    V = 5
    X = 10
    L = 50
    C = 100
    D = 500
    M = 1000


def is_valid_roman_number(number):
    pattern = re.compile(r"""   
                            ^M{0,3}
                            (CM|CD|D?C{0,3})?
                            (XC|XL|L?X{0,3})?
                            (IX|IV|V?I{0,3})?$
                        """, re.VERBOSE)

    return re.match(pattern, number)


class ReverseRoman:
    """ReverseRoman class"""

    def __init__(self):
        pass

    def convert(self, number_string):
        result = 0
        if is_valid_roman_number(number_string):
            last = 0
            for c in reversed(number_string):
                if last > ROMAN_NUM[c].value:
                    result = result - ROMAN_NUM[c].value
                else:
                    result = result + ROMAN_NUM[c].value
                last = ROMAN_NUM[c].value
        else:
            result = -1
        return result

