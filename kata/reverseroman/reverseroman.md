# Feature: Reverse Roman 

## Narrative

**As a** customer 
**I want** to convert Roman number to Arabic number
**So that** I could easily access its value 

## Scenario_1:

**Given**: Roman number with only one letter <br>
**When**: converted to Arabic number <br>
**Then**: value of both numbers should be same <br>

## Scenario_2:

**Given**: Roman number with multiple letters but every letter has less or equal value than its left neighbor <br>
**When**: converted to Arabic number <br>
**Then**: value of both numbers should be same <br>

## Scenario_3:

**Given**: Roman number with multiple letters and it is possible that some letters could have greater value than their left neighbor <br>
**When**: converted to Arabic number <br>
**Then**: value of both numbers should be same <br>

