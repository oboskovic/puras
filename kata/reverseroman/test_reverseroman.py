"""Test modul for Dog class"""

import unittest

from reverseroman import ReverseRoman


class TestReverseRoman(unittest.TestCase):
    """Test class for ReverseRoman class"""

    def setUp(self):
        self.reverseroman = ReverseRoman()

    def test_should_convert_one_letter_roman_number(self):
        """Testing Scenario_1, Number is successfully converted"""
        self.assertEqual(5, self.reverseroman.convert("V"))

    def test_should_report_usage_of_invalid_letter(self):
        """Testing Scenario_1, Input number has invalid letter"""
        self.assertEqual(-1, self.reverseroman.convert("R"))

    def test_should_convert_simple_roman_number_with_multiple_letters(self):
        """Testing Scenario_2, Number is successfully converted"""
        self.assertEqual(2023, self.reverseroman.convert("MMXXIII"))

    def test_should_report_invalid_roman_number(self):
        """Testing Scenarion_2, Input number is invalid"""
        self.assertEqual(-1, self.reverseroman.convert("MMMM"))

    def test_should_convert_roman_number_that_include_subtraction(self):
        """Testing Scenario_3, Number is successfully converted"""
        self.assertEqual(43, self.reverseroman.convert("XLIII"))


unittest.main()
